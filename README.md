# gemloginplace

Takes a collection of gemlog (Gemini log) posts and builds archives in-place (not with a source folder and a target folder) for time, tags and categories.

In case you're not familiar with it, Gemini is a a lightweight protocol that can be thought of as a more flexible Gopher or a vastly simplified web.

[Project Gemini](https://gemini.circumlunar.space/)

## Design goals

* Simple as possible! (This is for Gemini, after all.)
* Use the standard format for gemlogs so that every tag, category or archive page can be subscribed to.
[Subscribing to Gemini Pages](https://gemini.circumlunar.space/docs/companion/subscription.gmi)
* Minimize templating and external configs. The posts themselves are the configuration.
* Only change the archive files, avoiding any risk of clobbering the posts themselves. 
* Use a programming language that's already installed on most *nix systems. This is part of why I chose Perl instead of something newer like Rust or Go. (The other was that I already had an old site index generator that I could strip down and use as a basis for it.)

## How to use

Use the 2021-01-15-filename.gmi naming convention for post files. The date will be extracted from the filename.

Put them all in one folder.

The titles, tags, and categories will be extracted from the post files. To add a new tag or category to the gemlog, just add it to the posts and the script will find it the next time it's run. 

* The site title is the first top-level heading in index.gmi.
* The post title is the first top-level heading in the post.
* Links in each post to files in the "tag" or "category" subfolders will be used to build the tag/category lists and archives.

For example, the following markup will be a post with the title "Trying out Gemini". It will create a Gemini tag and a Technology category, and it will create archive files at tag/gemini.gmi and category/tech.gmi containing links back to this post.
```
# Trying out Gemini

Hey, look, I have a post!

=> tag/gemini.gmi Gemini
=> category/tech.gmi Technology

=> ./ Previous
=> ./ Next

Yeah, you can put stuff after the tag/category links, too.
```

Grab a copy of gemloginplace.pl from Codeberg:
[gemloginplace](https://codeberg.org/kvibber/gemloginplace)

Run gemloginplace.pl in the folder that contains your posts!

Next/Previous links are only updated in the posts if you specify --update-nav on the command-line.

The current version doesn't change your index.gmi file yet. I'm trying to decide on a good way to know what section to change without external templates. For now you can copy the top items from posts.gmi.

## CONFIG

***IN PROGRESS!***

gemloginplace.config

SITENAME: Your Site Name
HOME: Go Home


## TODO
Configuration options.
Documentation and more comments.

## History

2021-03-21 Version 0.2: Navigation

* Update Next/Previous links if present in posts, but only if --update-nav is specified.
* Get site name from index.
* Use a configuration file.
* Create tag and category subfolders if they don't already exist.

2021-03-04 Version 0.1: Initial release

## Contact

Kelson Vibber, kelson@pobox.com
* [Website](https://hyperborea.org)
* [Gemini capsule](gemini://hyperborea.org)
* [Fediverse/Mastodon: @KelsonV@Wandering.shop](https://wandering.shop/@kelsonv/)

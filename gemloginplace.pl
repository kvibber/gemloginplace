#!/usr/bin/perl -w

##############################################
# gemloginplace.pl by Kelson Vibber
# Version 0.2
# gemini://hyperborea.org
# https://hyperborea.org
# 
# Takes a collection of gemlog files and builds time,
# tag and category archives in place. Intended to be as
# simple as possible.
##############################################
use Cwd;
use strict;
#use Sort::Naturally;

# Get files in current folder
opendir(DIR, cwd());
my @FILES = sort(readdir(DIR));
closedir(DIR);

my %CONFIG;

# Get site name from index.gmi
if (-e "index.gmi") {
	open inFile, "index.gmi" || die("Cannot find index.gmi!\n");
	my @lines = <inFile>;
	close inFile;

	foreach my $line (@lines) {
		# The first first-level heading becomes the title.
		if ($line =~ /^# (.*)$/ && !exists($CONFIG{'SITENAME'}) ) {
			$CONFIG{'SITENAME'} = $1;
			# That's the only thing we're grabbing from index for now.
			last;
		}
	}
}

# Read config options if they exist.
if (-e "gemloginplace.config") {
	print STDERR "Loading labels from gemloginplace.config\n";
	open configFile, "gemloginplace.config";
	my @lines = <configFile>;
	foreach my $configLine(@lines) {
		if ($configLine =~ /^\s*([A-Za-z0-9_]+)\s*:\s*(.*)\s*$/) {
			print STDERR "config item $1 => $2\n";
			$CONFIG{$1}=$2;
		}
	}
	close configFile;
}

# Default configuration
if (!exists $CONFIG{'SITENAME'}) {
	$CONFIG{'SITENAME'} = "Gemlog";
}
if (!exists $CONFIG{'HOME'}) {
	$CONFIG{'HOME'} = "Back to $CONFIG{'SITENAME'}";
}
if (!exists $CONFIG{'ALLOW_WRITING_TO_POSTS'}) {
	$CONFIG{'ALLOW_WRITING_TO_POSTS'} = 0;
}

if ($ARGV[0] eq '--update-nav') {
	$CONFIG{'ALLOW_WRITING_TO_POSTS'} = 1;
}
if ($ARGV[0] eq '--update-nav-dry-run') {
	$CONFIG{'UPDATE_NAV_DRY_RUN'} = 1;
} else {
	$CONFIG{'UPDATE_NAV_DRY_RUN'} = 0;
}

my %tags;
my %categories;
my %tagPosts;
my %categoryPosts;
my %posts;
my %navLinks;

my $prev = "./";

# Read each post, building a representation of the files, titles, tags and categories
foreach my $file(@FILES) {

	# Only look at files with the pattern 2021-01-01-whatever-else.gmi
	if ($file !~ /^(\d\d\d\d\-\d\d-\d\d)-(.*)\.gmi$/) {
		next
	}
	my $date = $1;
	my $title = '';
	my $oldPrev = '';
	my $oldNext = '';

	print STDERR "Indexing $file\n";
	open inFile, $file || die("Cannot find $file!\n");
	my @lines = <inFile>;
	close inFile;

	my @postTags;
	my @postCategories;
	foreach my $line (@lines) {
		# The first first-level heading becomes the title.
		if ($line =~ /^# (.*)$/ && $title eq '') {
			$title = $1;
		}
		if ($line =~ /^=>\stag\/([^\s\.]*\.gmi)\s+(.*)$/) {
			push(@postTags, $1);
			if (!exists $tags{$1}) {
				$tags{$1} = $2;
			}
			if (!exists $tagPosts{$1}) {
				$tagPosts{$1} = [ $file ];
			} else {
				push @{ $tagPosts{$1} }, $file;
			}
		}
		if ($line =~ /^=>\scategory\/([^\s\.]*\.gmi)\s+(.*)$/) {
			push(@postCategories, $1);
			if (!exists $categories{$1}) {
				$categories{$1} = $2;
			}
			if (!exists $categoryPosts{$1}) {
				$categoryPosts{$1} = [ $file ];
			} else {
				push @{ $categoryPosts{$1} }, $file;
			}
		}
		if ($line =~ /^=>\s([^ ]+)\s+Previous(: .*)?$/) {
			$oldPrev = $line;
			chomp($oldPrev);
		}
		if ($line =~ /^=>\s([^ ]+)\s+Next(: .*)?$/) {
			$oldNext = $line;
			chomp($oldNext);
		}
	
	}
	if ($title eq '') {
		$posts{$file} = $file;
	} else {
		$posts{$file} = "$date $title";
	}
	%{$navLinks{$file}} = (
		'title'   => $title,
		'oldPrev' => $oldPrev,
		'oldNext' => $oldNext,
		'newNext' => ''
	);
	if (exists $navLinks{$prev}) {
		my $prevTitle = $navLinks{$prev}{'title'};
		$navLinks{$file}{'newPrev'} = "=> $prev Previous: $prevTitle";
		$navLinks{$prev}{'newNext'} = "=> $file Next: $title";
	} else {
		$navLinks{$file}{'newPrev'} = "=> $prev Previous: " . ${CONFIG}{'HOME'};
	}
	
	$prev = $file;
	print STDERR "     $title --- " .
		join(', ', @postCategories) .
		"\n     tags: " . join(', ', @postTags) . "\n";
	if ($oldPrev) {
		print STDERR  "     oldPrev: $oldPrev\n";
	}
	if ($oldNext) {
		print STDERR  "     oldNext: $oldNext\n";
	}
}


# Write the main index pages
writeIndex('posts.gmi', "$CONFIG{'SITENAME'}: All Posts", '', './', %posts);
writeIndex('categories.gmi', "$CONFIG{'SITENAME'}: Categories", 'category/', './', %categories);
writeIndex('tags.gmi', "$CONFIG{'SITENAME'}: Tags", 'tag/', './', %tags);

# Create folders if they don't exist
if (scalar(keys %categories) > 0 && ! -d "category") {
	print STDERR "Creating category folder\n";
	mkdir "category";
}
if (scalar(keys %tags) > 0 && ! -d "tag") {
	print STDERR "Creating tag folder\n";
	mkdir "tag";
}

# TODO generalize these since they're almost the same
foreach my $category (keys %categories) {
	print STDERR "Category $categories{$category} ($category) has...\n";
	my %localPosts;
	foreach my $postFile (@{ $categoryPosts{$category}}) {
		print "     $postFile\n";
		$localPosts{$postFile} = $posts{$postFile};
	}
	writeIndex("category/$category", "$categories{$category} Category - $CONFIG{'SITENAME'}", '../', '../', %localPosts);
}

foreach my $tag (keys %tags) {
	print STDERR "Tag $tags{$tag} ($tag) has...\n";
	my %localPosts;
	foreach my $postFile (@{ $tagPosts{$tag}}) {
		print "     $postFile\n";
		$localPosts{$postFile} = $posts{$postFile};
	}
	writeIndex("tag/$tag", "Posts Tagged $tags{$tag} - $CONFIG{'SITENAME'}", '../', '../', %localPosts);
}


# Go through navigation links and find the ones that need to be updated
foreach my $post (sort keys %navLinks) {
#	print STDERR "$post: ";
#	foreach my $key (keys %{$navLinks{$post}}) {
#		print STDERR "$key, ";
#	}
#	print STDERR "\n";
	my $oldPrev = $navLinks{$post}{'oldPrev'};
	my $newPrev = $navLinks{$post}{'newPrev'};
	my $oldNext = $navLinks{$post}{'oldNext'};
	my $newNext = $navLinks{$post}{'newNext'} || "=> ./ Next";
	my $changes = 0;


	if ($oldPrev and ($oldPrev ne $newPrev)) {
		$changes = 1;
	}
	if ($oldNext and ($oldNext ne $newNext)) {
		$changes = 1;
	}
	
	if ($changes) {
		#print STDERR " [$oldPrev] [$newPrev] [$oldNext]  [$newNext]  \n";
		if ($CONFIG{'ALLOW_WRITING_TO_POSTS'} eq 1 or $CONFIG{'UPDATE_NAV_DRY_RUN'} eq 1) {
			open inFile, $post || die("Cannot find $post!\n");
			my @lines = <inFile>;
			close inFile;
			foreach my $line (@lines) {
				if ($oldPrev and ($oldPrev ne $newPrev) and (index ($line, $oldPrev) > -1) ) {
					$line = "$newPrev\n";
					print STDERR "    update $post: was $oldPrev\n";
					print STDERR "    update $post: now $newPrev\n";
				}
				if ($oldNext and ($oldNext ne $newNext) and (index ($line, $oldNext) > -1) ) {
					$line = "$newNext\n";
					print STDERR "    update $post: was $oldNext\n";
					print STDERR "    update $post: now $newNext\n";
				}
			}
			if ($CONFIG{'ALLOW_WRITING_TO_POSTS'} eq 1) {
				open outFile, ">$post.new" || die("Cannot open $post.new for writing!\n");
				foreach my $line (@lines) {
					print outFile $line;
				}
				close outFile;
				my $differences = `diff -y --suppress-common-lines $post $post.new | wc -l`;
				chomp $differences;
				if ($differences <= 2) {
					unlink $post;
					rename ("$post.new", $post) || die "Failed to rename $post.new!\n";
				} else {
					print STDERR "Navigation changes in $post.new changed $differences lines. Not updating in case we broke it.\n";
				}
			}
		} else {
			print STDERR "$post has next/previous changes. Run with --update-nav to update it.\n";
		}
	}
}





# Write the index page for a tag, category, or just everything.
sub writeIndex {
	my ($file, $title, $prefix, $home, %posts) = @_;

	# Build archive page
	open outFile, ">$file" || die("Cannot open $file for writing!\n");
	print outFile "# $title\n\n";
	my $lastYear = '';
	my @list = sort (keys %posts);
	if ($list[0] =~ /^(\d\d\d\d)-/) {
		@list = reverse @list;
	}
	foreach my $postFile (@list) {
		if ($postFile =~ /^(\d\d\d\d)-/) {
			my $year = $1;
			if ($lastYear ne $year) {
				$lastYear = $year;
				print outFile "\n## $year\n\n";
			}
		}
		print outFile "=> $prefix$postFile $posts{$postFile}\n";
	}
	print outFile "\n=> $home $CONFIG{'HOME'}\n";
	close outFile;
}


